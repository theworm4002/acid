from feed import XmlFeed, get_json

cached_map = None

def get_map():
	return Map()

def get_cached_map():
	global cached_map

	if cached_map == None:
		cached_map = get_map()

	return cached_map

def get_country_name(s):
	map = get_cached_map()
	
	if isinstance(s, int):
		s = str(s)
	else:
		s = s.lower()
	
	for c in map.countries:
		if s == c['name'].lower() or s == c['code'].lower() or s == str(c['id']):
			return c['name']

	return None

#def get_country_link(name):
#	country_feed = country.get_cached_countries()
#
#	countries = country_feed.elements('/countries/country')
#	name = name.lower()
#
#	for c in countries:
#		if name == c.text('name').lower() or name == c.text('code').lower():
#			return c.text('permalink')
#
#	return None

def get_country_id(name):
	map = get_cached_map()
	name = name.lower()
	
	for c in map.countries:
		if name == c['name'].lower() or name == c['code'].lower():
			return c['id']

	return None

def get_country_currency(val):
	map = get_cached_map()
	countries = map.countries

	if isinstance(val, str) or isinstance(val, unicode):
		val = val.lower()
		for c in countries:
			if val == c['name'].lower() or val == c['code'].lower():
				return c['currency']
	elif isinstance(val, int):
		for c in countries:
			if val == c['id']:
				return c['currency']
	elif isinstance(val, dict):
		for c in countries:
			if val['id'] == c['id']:
				return c['currency']
	else:
		return None

def get_region_name(region_id):
	map = get_cached_map()
	for r in map.regions:
		if r['id'] == int(region_id):
			return r['name']
		
	return None

def get_region_id(region_name):
	map = get_cached_map()

	for r in map.regions:
		if r['name'].lower().startswith(region_name.lower()):
			return r['id']

	return None

def get_region(search):
	map = get_cached_map()
	search = str(search)
	search = search.lower()
	for r in map.regions:
		if str(r['id']) == search or r['name'].lower().startswith(search):
			return r
	
	return None

def get_region_zone(region_name):
	map = get_cached_map()
	for region in map.regions:
		if region['name'].lower().startswith(region_name.lower()):
			return region['zone']
	return None

def get_region_distance(start, end):
	x1, x2 = start.lower()
	y1, y2 = end.lower()
	x2 = int(x2)
	y2 = int(y2)
	dist_x = abs(ord(x1) - ord(y1))
	if dist_x == 3:
		dist_x = 1
	dist_y = abs(x2 - y2)
	if dist_y == 3:
		dist_y = 2
	elif dist_y == 4:
		dist_y = 1
	return dist_x + dist_y + 1

def get_battles(searchstr):
	searchstr = searchstr.lower()
	f = XmlFeed('http://api.erepublik.com/map/data/')
	battles = []

	for battle in f.elements('/countries/country/regions/region/battles/battle'):
		battle_id = int(battle.attribute('.', 'b_id'))
		attacker_id = int(battle.attribute('.', 'c_id'))
		attacker = get_country_name(attacker_id)
		region_id = battle.attribute('../..', 'r_id')
		region = get_region_name(region_id)
		defender_id = int(battle.attribute('../../../..', 'c_id'))
		defender = get_country_name(defender_id)
		if not searchstr or searchstr in (attacker.lower(), region.lower(), defender.lower()):
			battles.append({
				'battle_id':   battle_id,
				'attacker_id': attacker_id,
				'attacker':    attacker,
				'region_id':   region_id,
				'region':      region,
				'defender_id': defender_id,
				'defender':    defender,
			})

	return battles

def get_mpp_list(country_id):
	map = XmlFeed('http://api.erepublik.com/map/data/')

	mpps = []
#	for mpp in map.elements('/countries/country[@c_id = "%d"]/mpps/mpp' % country_id): bug with pypy 1.5.0-alpha0 / python 2.7.1
	for country in map.elements('/countries/country'):
		if int(country.attribute('.', 'c_id')) == int(country_id):
			for mpp in country.elements('./mpps/mpp'):
				mpps.append({
					'id': int(mpp.attribute('.', 'c_id')), 'country': get_country_name(int(mpp.attribute('.', 'c_id'))),
					'expiration': mpp.attribute('.', 'expires')})

	return mpps

class Map:
	def __init__(self):
		data = get_json('http://api.1way.it/erep/map')
		self.countries = data['countries']
		self.regions = data['regions']
