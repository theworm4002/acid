import feed
from feed import XmlFeed

def get(buy, sell):
	return Exchange(XmlFeed('http://api.erepublik.com/v2/feeds/exchange/%s/%s' % (buy, sell)))

class Exchange:
	"""Erepublik monetary market"""
	
	def __init__(self, f):
		self.records = [{
			'amount': record.decimal('amount'),
			'price': record.decimal('exchange-rate'),
			'id': record.int('seller//id'),
			'name': record.text('seller//name')}
			for record in f.elements('/offers/offer')]

