from feed import XmlFeed
from urllib import urlencode

class Wolfram(object):
	API_URL = 'http://api.wolframalpha.com/v2/query?'
	
	def __init__(self, key):
		self.API_KEY = key
	
	def alpha(self, query):
		u = self.API_URL + urlencode({
									  'input': query,
									  'appid': self.API_KEY,
									  'format': 'plaintext',
									 })
		data = XmlFeed(u)
		if data.attribute('/queryresult', 'success') != 'true':
			return None
		
		try:
			q = data.text('/queryresult/pod[1]/subpod/plaintext')
			r = data.text('/queryresult/pod[2]/subpod/plaintext')
			assert q
			return (q, r, )
		except:
			return None
