class CommandManager(object):
	def __init__(self):
		self.prefix = self.get_prefix()
		#self.invalid = self.get_invalid()
		self.commands = self.get_commands()
		self.generate_help()

	def get_command(self, command):
		command = command.lower()

		if not command.startswith(self.prefix):
			return None

		command = command[len(self.prefix):]

		if not command in self.commands:
			return None

		command = self.commands[command]

		if not isinstance(command, basestring):
			return command

		command = command.lower()

		if not command in self.commands:
			return None

		return self.commands[command]

	def get_commands(self):
		return {}

	def generate_help(self):
		self.help = []

#		self.help.append('@bCommands@b (type @b%shelp command name@b for detailed information):' % self.prefix)
#		self.help.append(' ')

		longest = 0
		alias_dict = {}
		commands = {}

		for cmd in self.commands:
			if isinstance(self.commands[cmd], basestring):
				orig = self.commands[cmd]

				if orig in alias_dict:
					alias_dict[orig].append(cmd)
				else:
					alias_dict[orig] = [cmd]
			else:
				if not cmd in alias_dict:
					alias_dict[cmd] = []

		for key in alias_dict:
			cur = key + ('' if len(alias_dict[key]) == 0 else (' (' + ', '.join(alias_dict[key]) + ')'))
			longest = len(cur) if len(cur) > longest else longest
			commands[cur] = self.commands[key][1]

		for cmd in sorted(commands):
			self.help.append('@b%-*s@b   %s' % (longest + 1, cmd, commands[cmd]))
