package net.rizon.acid.plugins;

import java.util.jar.Manifest;
import org.eclipse.aether.artifact.Artifact;

public abstract class Plugin
{
	private String name;
	private boolean permanent;
	public ClassLoader loader; // Loader for this plugin
	protected Artifact artifact;
	protected Manifest manifest;

	protected void setPermanent()
	{
		permanent = true;
	}

	public boolean isPermanent()
	{
		return permanent;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Artifact getArtifact()
	{
		return artifact;
	}

	public abstract void start() throws Exception;
	public abstract void stop();
	public void reload() throws Exception { }
}
