package net.rizon.acid.commands;

import java.util.Iterator;
import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.Protocol;
import net.rizon.acid.core.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Shuts acid4 down gracefully.
 */
public class Shutdown extends Command
{
	private static final Logger log = LoggerFactory.getLogger(Shutdown.class);

	public Shutdown()
	{
		super(0, 512);
	}

	@Override
	public void Run(User x, AcidUser to, Channel c, final String[] args)
	{
		/* Send command usage to log channel; after this command runs, there
		 * won't be a socket to send the message through.
		 */
		Acidictive.privmsg(Acidictive.conf.getChannelNamed("cmdlogchan"), x.getNick() + "->" + to.getNick() + ": " + "shutdown");
		Protocol.wallop(to.getUID(), "SHUTDOWN command from " + x.getNick());
		final String quitReason = "Quit: " + (args.length < 1 ? "SHUTDOWN requested" : String.join(" ", args));		

		/* Given DB changes are always written when they occur, all we need to
		 * flush are our SQL and socket buffers.
		 */
		Acidictive.acidcore_sql.shutdown();

		for (User u : User.getUsersC())
		{
			if (u == null || u.getServer() != AcidCore.me)
				continue;

			/* Fake quit because we can't AcidUser.quit due to concurrent
			 * modification exceptions.
			 */
			Protocol.quit(u, quitReason);
		}

		// SQUIT our uplink or it won't be all that graceful of a shutdown
		Protocol.squit(AcidCore.me, "SHUTDOWN command from " + x.getNick());

		AcidCore.shutdown();

		System.exit(0);
	}

	@Override
	public void onHelp(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "\2shutdown [reason]\2 / Shuts everything down");
	}

	@Override
	public boolean onHelpCommand(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "Syntax: \2shutdown [reason]\2");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "The shutdown command shuts the pseudoserver down gracefully.");
		Acidictive.reply(u, to, c, "This includes flushing all remaining SQL queries, sending an SQUIT,");
		Acidictive.reply(u, to, c, "flushing and closing all sockets.");
		return true;
	}
}
