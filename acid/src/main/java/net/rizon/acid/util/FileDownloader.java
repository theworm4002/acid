/*
 * Copyright (c) 2017, Orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.zip.GZIPInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Orillion <orillion@rizon.net>
 */
public class FileDownloader
{
	private static final Logger LOGGER = LoggerFactory.getLogger(FileDownloader.class);
	private static final int CONNECT_TIMEOUT = 1000;
	private static final int READ_TIMEOUT = 1000;

	public enum CompressionMethod
	{
		GZIP,
		NONE,
	}

	/**
	 * Downloads a file from the specified url and decompresses it to the
	 * destination.
	 *
	 * @param url         URL to download from
	 * @param compression Compression method of the file
	 * @param destination Destination to write to
	 *
	 * @return true if successful, false otherwise
	 */
	public static boolean download(String url, CompressionMethod compression, File destination)
	{
		try
		{
			URL site = new URL(url);
			switch (compression)
			{
				case GZIP:
					gzipDownload(site, destination);
					break;
				case NONE:
					noneDownload(site, destination);
					break;
				default:
					LOGGER.warn("Unknown compression method");
					break;
			}
		}
		catch (IOException ex)
		{
			LOGGER.error("Error while downloading file from url ["
					+ url.toString()
					+ "] to destination ["
					+ destination.toString()
					+ "] with compression ["
					+ compression.toString()
					+ "]", ex);
			return false;
		}

		return true;
	}

	private static void noneDownload(URL url, File destination) throws IOException
	{
		URLConnection conn = url.openConnection();
		conn.setConnectTimeout(CONNECT_TIMEOUT);
		conn.setReadTimeout(READ_TIMEOUT);

		try (InputStream stream = conn.getInputStream();
				FileOutputStream fos = new FileOutputStream(destination);
				ReadableByteChannel channel = Channels.newChannel(stream))
		{
			fos.getChannel().transferFrom(channel, 0, Long.MAX_VALUE);
		}
	}

	private static void gzipDownload(URL url, File destination) throws IOException
	{
		URLConnection conn = url.openConnection();
		conn.setConnectTimeout(CONNECT_TIMEOUT);
		conn.setReadTimeout(READ_TIMEOUT);

		try (InputStream stream = conn.getInputStream();
				GZIPInputStream gis = new GZIPInputStream(stream);
				FileOutputStream fos = new FileOutputStream(destination);
				ReadableByteChannel channel = Channels.newChannel(gis))
		{
			fos.getChannel().transferFrom(channel, 0, Long.MAX_VALUE);
		}
	}
}
