package net.rizon.acid.messages;

import java.net.InetAddress;
import java.net.UnknownHostException;
import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;
import net.rizon.acid.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Encap extends Message
{
	private static final Logger log = LoggerFactory.getLogger(Encap.class);

	public Encap()
	{
		super("ENCAP");
	}
	
	@Override
	public void onUser(User user, String[] params)
	{
		if (params.length < 2)
		{
			return;
		}

		processEncap(user, null, params);
		broadcastEncap(user, null, params);
	}

	@Override
	public void onServer(Server source, String[] params)
	{
		if (params.length < 2)
		{
			return;
		}

		processEncap(null, source, params);
		broadcastEncap(null, source, params);
	}

	private void broadcastEncap(User sourceUser, Server sourceServer, String[] params)
	{
		String target = params[0];
		String command = params[1].toUpperCase();
		String[] args = new String[params.length - 2];
		System.arraycopy(params, 2, args, 0, params.length - 2);

		if (target.equals("*"))
		{
			// ENCAP is targetted at all servers
			Acidictive.onEncap(sourceServer, sourceUser, null, command, args);
		}
		else
		{
			Server server = Server.findServer(target);

			if (server != null)
			{
				Acidictive.onEncap(sourceServer, sourceUser, server, command, args);
			}
		}
	}
	
	private void processEncap(User sourceUser, Server source, String[] params)
	{
		if (!Util.iswm(params[0], AcidCore.me.getName()) && !params[1].equalsIgnoreCase(AcidCore.me.getSID()))
			;
		else if (params[1].equalsIgnoreCase("SVSMODE"))
		{
			// :services.rizon.net ENCAP * SVSMODE weed 1145782805 +rd :1145782805
			// :im.a.server ENCAP * SVSMODE dizzy 1145772712 :+r

			User target = User.findUser(params[2]);
			if (target == null)
				return;

			String modes = params[4];
			modes = modes.replaceAll("d", ""); // Guess we don't track service stamps?
			target.setMode(modes);
		}
		else if (params[1].equalsIgnoreCase("CHGHOST"))
		{
			// :services.rizon.net ENCAP * CHGHOST NocturneXDCC :is.a.yote.server

			User user = User.findUser(params[2]);
			if (user == null)
				return;

			user.setVhost(params[3]);
		}
		else if (params[1].equals("SU"))
		{
			User user = User.findUser(params[2]);
			if (user == null)
				return;

			if (params.length > 3)
				user.setSU(params[3]);
			else
				user.setSU("");
		}
		else if (params[1].equals("CERTFP"))
		{
			User user = User.findUser(params[2]);
			if (user == null)
				return;

			user.setCertFP(params[3]);
		}
		else if (params[1].equals("SNOMASK"))
		{
			/*
			 * currently has this format but may as well future proof
			 * it in case we decide to send an empty one instead of 
			 * just +. for now treat them as malformed.
			 * 
			 * :0AA ENCAP * SNOMASK 0AAAAAAAA :+
			 */
			if (params.length < 3)
			{
				log.warn("Snomask: Received malformed ENCAP SNOMASK from {}",
					source != null ? source.getName() : "<unknown>");

				return;
			}

			User user = User.findUser(params[2]);
			if (user == null)
			{
				log.warn("Snomask: Unable to find user {}", params[2]);
				return;
			}

			user.setSnomask(params[3]);
			log.debug("Snomask for {} changed to {}", user.getNick(), user.getSnomask());
		}
		else if (params[1].equals("AUTHFLAGS"))
		{
			User user = User.findUser(params[2]);
			if (user == null)
				return;

			user.setAuthFlags(params[3]);
		}
		else if (params[1].equals("SWEBIRC"))
		{
			int parc = params.length;
			if (parc < 10 || source == null)
				return;
			
			Acidictive.onWebIRC(source,
				params[2], // operation
				params[3], // uid
				params[4], // realhost
				params[5], // sockhost
				params[parc - 4], // webirc password
				params[parc - 3], // webirc username
				params[parc - 2], // requested host
				params[parc - 1] // requested ip
			);
		}
		else if (params[1].equals("CHGREALHOST"))
		{
			User user = User.findUser(params[2]);
			if (user == null)
				return;

			user.setRealhost(params[3], params[4]);
		}
		else if (params[1].equals("UWEBIRC") && params.length == 5)
		{
			User user = User.findUser(params[2]);
			if (user == null)
			{
				return;
			}

			InetAddress address;

			try
			{
				address = InetAddress.getByName(params[3]);
			}
			catch (UnknownHostException ex)
			{
				return;
			}

			String host = params[4];

			user.setCgisockhost(address);
			user.setCgihost(host);
		}
	}
}
