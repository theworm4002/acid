package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Part extends Message
{
	private static final Logger log = LoggerFactory.getLogger(Part.class);

	public Part()
	{
		super("PART");
	}

	// :99hAAAAAB PART #geo

	@Override
	public void onUser(User u, String[] params)
	{
		Channel chan = Channel.findChannel(params[0]);
		if (chan == null)
		{
			log.warn("PART from " + u.getNick() + " for nonexistent channel " + params[0]);
			return;
		}

		chan.removeUser(u);
		u.remChan(chan);

		Acidictive.onPart(u, chan);
	}
}