package net.rizon.acid.events;

import net.rizon.acid.core.User;

public class EventUserConnect
{
	private User user;

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}
}
