package net.rizon.acid.events;

import java.util.List;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.ChannelModeChange;

public class EventChanMode
{
	private String prefix;
	private Channel chan;
	private String modes;

	public void setUserModes(List<ChannelModeChange> userModes)
	{
		this.userModes = userModes;
	}

	public List<ChannelModeChange> getUserModes()
	{

		return userModes;
	}

	private List<ChannelModeChange> userModes;

	public String getPrefix()
	{
		return prefix;
	}

	public void setPrefix(String prefix)
	{
		this.prefix = prefix;
	}

	public Channel getChan()
	{
		return chan;
	}

	public void setChan(Channel chan)
	{
		this.chan = chan;
	}

	public String getModes()
	{
		return modes;
	}

	public void setModes(String modes)
	{
		this.modes = modes;
	}
}
