package net.rizon.acid.core;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import java.time.Duration;
import java.util.Arrays;
import net.rizon.acid.io.IRCMessage;
import net.rizon.acid.io.Initializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AcidCore
{
	private static final Logger log = LoggerFactory.getLogger(AcidCore.class);
	
	private static final Duration CONNECT_TIMEOUT = Duration.ofSeconds(5);

	public static Server me;
	public static boolean ssl;
	public static String uplink, password;
	public static int port;

	protected static final EventLoopGroup eventLoop = new NioEventLoopGroup(1);
	private static io.netty.channel.Channel channel;

	public static void start(String server, int port, String name, String description, String password, String SID, boolean ssl)
	{
		me = new Server(name, null, description, 0, SID);

		AcidCore.uplink = server;
		AcidCore.password = password;
		AcidCore.port = port;
		AcidCore.ssl = ssl;
	}

	private static void connect() throws InterruptedException
	{
		Bootstrap bootstrap = new Bootstrap()
			.group(eventLoop)
			.channel(NioSocketChannel.class)
			.handler(new Initializer())
			.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, (int) CONNECT_TIMEOUT.toMillis());

		ChannelFuture future = bootstrap.connect(uplink, port);
		channel = future.channel();

		future.await();

		if (future.isSuccess() == false)
		{
			log.warn("unable to connect", future.cause());
		}
	}

	protected static void send(IRCMessage message)
	{
		channel.writeAndFlush(message);
	}

	public static void onConnect()
	{
		Protocol.uplink(me, password);

		Acidictive.onStart();
	}

	public static void onDisconnect()
	{
		channel.close();

		log.info("Disconnected");
	}

	public static void shutdown()
	{
		channel.flush();
		try
		{
			channel.close().await();
		}
		catch (InterruptedException ex)
		{
			log.warn(null, ex);
		}
	}

	public static void processMessage(IRCMessage message)
	{
//		if (Acidictive.conf.protocol_debug)
//		{
//			BufferedWriter b = new BufferedWriter(new FileWriter("acid.protocol.debug", true));
//			b.write(System.currentTimeMillis() + " " + str + "\n");
//			b.close();
//		}

		Message m = Message.findMessage(message.getCommand());
		if (m == null)
		{
			log.debug("Unknown message " + message.getCommand());
			return;
		}

		String source = message.getSource();
		String[] params = Arrays.stream(message.getParams()).map(o -> o.toString()).toArray(i -> new String[i]);

		Server server = null;
		User user = null;

		if (source != null)
		{
			server = Server.findServer(source);
			if (server == null)
			{
				user = User.findUser(source);
			}
		}

		if (user != null)
		{
			m.onUser(user, params);
		}
		else if (server != null)
		{
			m.onServer(server, params);
		}

		m.on(source, params);
	}

	public static void run() throws InterruptedException
	{
		connect();

		channel.closeFuture().awaitUninterruptibly();
	}

	public static int getTS()
	{
		return (int) (System.currentTimeMillis() / 1000);
	}

	public static void killNick(String nick, String reason)
	{
		Protocol.kill(nick, reason);

		User x = User.findUser(nick);
		if (x != null)
		{
			x.onQuit();
		}
	}

	public static String arrayFormat(String[] sA, int start, int end)
	{
		StringBuffer str = new StringBuffer();
		if (end >= sA.length)
			return null;
		for (int i = start; i <= end; i++)
		{
			if (str.length() > 0)
				str.append(" ");
			str.append(sA[i]);
		}
		return str.toString();
	}




}
