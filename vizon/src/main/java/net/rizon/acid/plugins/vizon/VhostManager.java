/*
 * Copyright (c) 2017, orillion <orillion@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.plugins.vizon;

import net.rizon.acid.plugins.vizon.db.VizonRequest;
import net.rizon.acid.plugins.vizon.db.VizonUser;
import java.util.regex.Pattern;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Protocol;
import net.rizon.acid.core.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author orillion <orillion@rizon.net>
 */
public class VhostManager
{
	private static final Logger logger = LoggerFactory.getLogger(VhostManager.class);
	private static final String DELETE_FORMAT = "DEL %s";

	public static final int MAX_HOST_BYTES = 63;
	public static final Pattern NORMAL_PATTERN = Pattern.compile("^[\u0003\u000Fa-zA-Z0-9-.]*[a-zA-Z]+[\u0003\u000Fa-zA-Z0-9-.]*$");
	public static final Pattern BOLD_PATTERN = Pattern.compile("^[\u0003\u0002\u000Fa-zA-Z0-9-.]*[a-zA-Z]+[\u0003\u0002\u000Fa-zA-Z0-9-.]*$");

	public VhostManager()
	{

	}

	public boolean assignVhost(VizonRequest request)
	{
		VizonUser user = Vizon.getVizonDatabase().findUserById(request.getUserId());

		if (user == null)
		{
			return false;
		}

		Acidictive.privmsg("HostServ", String.format(DELETE_FORMAT, user.getNick()));

		user.setVhost(request.getVhost());
		user.setEligible(false);

		if (!Vizon.getVizonDatabase().updateUser(user))
		{
			return false;
		}

		User u = User.findUser(request.getNick());

		if (u != null)
		{
			this.applyVhostIfApplicable(u);
		}

		return true;
	}

	public void applyVhostIfApplicable(User user)
	{
		if (user == null || !user.hasMode("r"))
		{
			return;
		}

		VizonUser u = Vizon.getVizonDatabase().findUser(user.getNick());

		if (u == null || u.getVhost() == null || u.getObtained() == null)
		{
			return;
		}

		Protocol.chghost(user, u.getVhost());
		u.setVhost(u.getVhost());
	}

	public void expireVhosts()
	{
		int expired = Vizon.getVizonDatabase().expireVhosts();

		// Users can keep their colored vhosts until they log out, when they expire.
		logger.info("Expired {} vhosts", expired);
	}
}
