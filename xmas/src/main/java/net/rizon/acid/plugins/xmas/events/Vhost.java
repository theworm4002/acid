package net.rizon.acid.plugins.xmas.events;

import net.rizon.acid.core.Protocol;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.xmas.Xmas;
import net.rizon.acid.util.Format;

public class Vhost implements Runnable
{
	private static final int MAX_VHOST = 63;
	
	private final User user;
	private final boolean special;

	/**
	 * Set a seasonal vhost on a user
	 *
	 * @param user User Which user to change the host of
	 * @param special
	 */
	public Vhost(User user, boolean special)
	{
		this.user = user;
		this.special = special;
	}

	@Override
	public void run()
	{
		String vhost = Xmas.todaysVhost();

		// No vhost for today
		if (vhost == null)
			return;

		String prefix = "";

		if (this.special)
		{
			final double rand = Math.random();
			if (rand <= 0.10)
			{
				prefix = "" + Format.BOLD + Format.UNDERLINE;
			}
			else if (rand <= 0.40)
			{
				prefix = "" + Format.BOLD;
			}
			else
			{
				prefix = "" + Format.UNDERLINE;
			}
		}
		vhost = prefix + vhost + prefix;
		String nick = this.user.getNick();
		nick = nick.replaceAll("[^a-zA-Z0-9-]", "");
		vhost += nick;

		vhost = vhost.substring(0, Math.min(vhost.length(), MAX_VHOST));
		Protocol.chghost(this.user, vhost);
	}
}